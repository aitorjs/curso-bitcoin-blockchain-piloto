const describe = require('mocha').describe
const it = require('mocha').it
const expect = require('chai').expect

const Utils = require('../Utils')
const Http = require('../http')
const { bank, alice, bob } = Utils.makeBankAnd2address()
const { Block, Transaction, Bank } = require('../UTXOCoin')

describe('Integration tests', () => {

  let { blockchain } = ourbank = new Bank()
  const http = new Http(blockchain)

  it('Make first good block, #1', done => {
    const tx = new Transaction()
    const output1 = { address: alice.pubkey, value: 10.00000000, n: 0, fee: 1.00000000 }
    const isValidTx = tx.createtx(blockchain.utxo[0], output1, blockchain, false, Utils.now())
    expect(isValidTx).to.be.equal(true)
    
    tx.signTx(bank.privkey)
    blockchain.mempool.push(tx)
    
    const block1 = {
      index: blockchain.getLastBlock().header.index + 1,
      timestamp: Utils.now(),
      transactions: blockchain.mempool
    }
    
    const candidateblock1 = new Block({ index: block1.index, timestamp: block1.timestamp }, block1.transactions)
    const res = blockchain.addBlock(candidateblock1)

    expect(res).to.be.equal(true)

    const savedBlock = blockchain.chain[1]
    expect(savedBlock.header.index).to.be.equal(1)
    expect(savedBlock.header.timestamp).to.be.equal(Utils.now())
    expect(savedBlock.transactions.length).to.be.equal(2)
    
    expect(blockchain.mempool.length).to.be.equal(0)

    expect(blockchain.getBalance(bank.pubkey.toString('hex'))).to.be.equal(90)
    expect(blockchain.getBalance(alice.pubkey)).to.be.equal(10)
    expect(blockchain.getBalance(bob.pubkey)).to.be.equal(0)
   
    done()
  })

 it('Make second good block, #2', done => {
    const tx2 = new Transaction()
    const output2 = { address: bob.pubkey, value: 5.00000000, n: 0, fee: 1.00000000 }
    const isValidTx = tx2.createtx(blockchain.utxo[1], output2, blockchain, false, Utils.now())
    expect(isValidTx).to.be.equal(true)

    tx2.signTx(alice.privkey)
    blockchain.mempool.push(tx2)

    const block2 = {
      index: blockchain.getLastBlock().header.index + 1,
      timestamp: Utils.now(),
      transactions: blockchain.mempool
    }
    
    const candidateblock2 = new Block({ index: block2.index, timestamp: block2.timestamp }, block2.transactions)
    const res = blockchain.addBlock(candidateblock2)

    expect(res).to.be.equal(true)

    const savedBlock = blockchain.chain[2]
    expect(savedBlock.header.index).to.be.equal(2)
    expect(savedBlock.header.timestamp).to.be.equal(Utils.now())
    expect(savedBlock.transactions.length).to.be.equal(2)
    
    expect(blockchain.mempool.length).to.be.equal(0)

    finalBalance()

    done()
  })

  it('Bad signature transaction', done => {
    const tx3 = new Transaction()
    const output3 = { address: bob.pubkey, value: 3.00000000, n: 0, fee: 1.00000000 }

    const isValidTx = tx3.createtx(blockchain.utxo[0], output3, blockchain, false, Utils.now() - 1)
    expect(isValidTx).to.be.equal(true)
    
    tx3.signTx(bob.privkey) // alice
    blockchain.mempool.push(tx3)
    
    const block3 = {
        index: blockchain.getLastBlock().header.index + 1,
        timestamp: Utils.now(),
        transactions: blockchain.mempool
    }
    
    const candidateblock3 = new Block({ index: block3.index, timestamp: block3.timestamp }, block3.transactions)
    const res = blockchain.addBlock(candidateblock3)

    expect(res.type).to.be.equal('id-tx-invalid')
    
    finalBalance()

    done()
  })

  it('Without amount, without UTXO', done => {
    const random = Utils.newAddress()

    const tx4 = new Transaction()
    const output4 = { address: random.pubkey, value: 1.00000000, n: 0, fee: 1.00000000 }
    
    const isValidTx = tx4.createtx(blockchain.utxo[0], output4, blockchain, false, Utils.now() - 1)
    expect(isValidTx).to.be.equal(true)

    tx4.signTx(random.privkey) // miner
    blockchain.mempool.push(tx4)
    
    const block4 = {
      index: blockchain.getLastBlock().header.index + 1,
      timestamp: Utils.now(),
      transactions: blockchain.mempool
    }
    
    const candidateblock4 = new Block({ index: block4.index, timestamp: block4.timestamp }, block4.transactions)
    const res = blockchain.addBlock(candidateblock4)
    expect(res.type).to.be.equal('id-tx-invalid')

    finalBalance()
    
    done()
  })

  it('Too much amount', done => {
    const tx5 = new Transaction()
    const output5 = { address: alice.pubkey, value: 5.00000000, n: 0, fee: 1.00000000 }
    
    const isValidTx = tx5.createtx(blockchain.utxo[3], output5, blockchain, false, Utils.now() - 1)
    expect(isValidTx.type).to.be.equal('less-balance')

    finalBalance()
    
    done()
  })

  it('Tx locktime after blocktime', done => {
    const tx6 = new Transaction()
    const output6 = { address: alice.pubkey, value: 4.00000000, n: 0, fee: 1.00000000 }
    
    const isValidTx = tx6.createtx(blockchain.utxo[3], output6, blockchain, false, Utils.now() + 1)
    expect(isValidTx).to.be.equal(true)

    tx6.signTx(bob.privkey)
    blockchain.mempool.push(tx6)
    
    const block6 = {
        index: blockchain.getLastBlock().header.index + 1,
        timestamp: Utils.now(),
        transactions: blockchain.mempool
    }
    
    const candidateblock6 = new Block({ index: block6.index, timestamp: block6.timestamp }, block6.transactions)
    const res = blockchain.addBlock(candidateblock6)
    
    expect(res.type).to.be.equal('invalid-tx-locktime')

    finalBalance()

    done()
  })

  function finalBalance() {
    expect(blockchain.getBalance(bank.pubkey.toString('hex'))).to.be.equal(141)
    expect(blockchain.getBalance(alice.pubkey)).to.be.equal(4)
    expect(blockchain.getBalance(bob.pubkey)).to.be.equal(5)
  }
})
