const Crypto = require('crypto')
const Utils = require('./Utils')
const genesisTimestamp = Utils.now()
const secp256k1 = require('secp256k1')

class Block {
  constructor (header, transactions, pubkey = bank.bankPubKeyHex, signature = '') {
    this.header = {}
    this.header.index = header.index
    this.header.timestamp = header.timestamp
    this.header.previousHash = ''
    this.transactions = transactions
    this.pubkey = pubkey
    this.signature = signature
    
    this.hash = this.calculateHash()
  }
  
  calculateHash() {
    const str = JSON.stringify(this.header)

    return Crypto.createHash('sha256').update(str).digest('hex')
  }
  
  signBlock(privkey) {
    privkey = Buffer.from(privkey, 'hex')
    
    const str = JSON.stringify(this.header) +
    JSON.stringify(this.transactions) + this.pubkey + this.hash
    const msg = Buffer.from(Crypto.createHash('sha256').update(str).digest('hex'), 'hex')
    
    return secp256k1.sign(msg, privkey)
  }
}

class Transaction {
  constructor(from, to, amount) {
    this.from = from
    this.to = to
    this.amount = amount
  }
}

class Blockchain {
  constructor() {
    this.chain = []

    const genesisBlock = this.createGenesisBlock()
    this.chain.push(genesisBlock)
  }
  
  createGenesisBlock() {
    const genesisTx = new Transaction(null, 'miner', 50)
    
    return new Block({ index: 0, timestamp: genesisTimestamp }, [genesisTx], 'pubkey', 'signature')
  }
  
  getLastBlock() {
    return this.chain[this.chain.length - 1]
  }
  
  addBlock(newBlock, privkey = bank.bankPrivKey) {
    newBlock.header.previousHash = this.getLastBlock().hash
    newBlock.hash = newBlock.calculateHash()
    
    const { signature } = newBlock.signBlock(privkey)
    newBlock.signature = signature.toString('hex')
    
    if (bank.isBlockValid(newBlock)) {
      this.chain.push(newBlock)
    } else {
      console.log('Bloque no válido. Se olvida. No se almacena')
    }
  }
}

// Node on the next classes
class Bank {
  constructor() {
    this.blockchain = new Blockchain()

    this.bankPrivKey = Crypto.randomBytes(32)
    this.bankPubKey = secp256k1.publicKeyCreate(this.bankPrivKey)
    this.bankPubKeyHex = this.bankPubKey.toString('hex')
  }

  isBlockValid(newBlock) {
    const previousBlock = this.blockchain.getLastBlock()
    
    return this.validations(newBlock, previousBlock)
  }
  
  validations(newBlock, previousBlock) {
    try {
      const str = JSON.stringify(newBlock.header) +
      JSON.stringify(newBlock.transactions) + newBlock.pubkey +
      newBlock.hash
      const msg = Buffer.from(Crypto.createHash('sha256').update(str).digest('hex'),'hex')

      const signature = Buffer.from(newBlock.signature, 'hex')
      const pubkey = Buffer.from(newBlock.pubkey, 'hex')
    
      this.isValidBlockHash(newBlock)
      this.isValidPreviousHash(newBlock, previousBlock)
      this.isValidSig(msg, signature, pubkey)
      return this.isValidBankSig(msg, signature, this.bankPubKey)
    } catch (err) {
      console.log('ERROR', err)
      return false
    }
  }

  isValidGenesisBlock() {
    const memoryGenesisBlock = JSON.stringify(this.blockchain.chain[0])
    const genesisBlock = JSON.stringify(this.blockchain.createGenesisBlock())
    
    if (memoryGenesisBlock !== genesisBlock) {
      throw 'Error: Invalid block: genesis block'
    }

    return true
  }

  isValidPreviousHash(newBlock, previousBlock) {
    if (newBlock.header.previousHash !== previousBlock.calculateHash()) {
      throw 'Error: Invalid block: hashPreviousBlock'
    }

    return true
  }

  isValidBlockHash(newBlock) {
    if (newBlock.hash !== newBlock.calculateHash()) {
      throw 'Error: Invalid block: block hash'
    }

    return true
  }

  isValidSig(msg, signature, pubkey) {
    if (!secp256k1.verify(msg, signature, pubkey)) {
      throw 'Error: not signed by the correct privKey'
    }

    return true
  }

  isValidBankSig(msg, signature, bankpubkey) {
    if (!secp256k1.verify(msg, signature, bankpubkey))   {
      throw 'Error: not signed by the bank'
    }

    return true
  }
  
  isChainValid() {
    if (!this.isValidGenesisBlock()) return false

    for (let i = 1; i < this.blockchain.chain.length; i++) {
      const currentBlock = this.blockchain.chain[i]
      const previousBlock = this.blockchain.chain[i - 1]
      
      let isValidBlock = this.validations(currentBlock, previousBlock)
      
      if (!isValidBlock) return false
    }
    
    return true
  }
}

function main() {
  let { blockchain } = bank = new Bank()

  console.log('----- Añadir dos bloques ----')

  const block1 = {
    index: blockchain.getLastBlock().header.index + 1,
    timestamp: Utils.now(),
    transactions: [new Transaction('miner', 'address1', 10)]
  }
  blockchain.addBlock(new Block({ index: block1.index, timestamp: block1.timestamp }, block1.transactions))

  const block2 = {
    index: blockchain.getLastBlock().header.index + 1,
    timestamp: Utils.now(),
    transactions: [new Transaction('address1', 'address2', 10)]
  }
  blockchain.addBlock(new Block({ index: block2.index, timestamp: block2.timestamp }, block2.transactions))
  
  console.log(bank.isChainValid() ? 'Blockchain valida' : 'Blockchain NO valida')

  console.log('----- Bad signature ----')

  const block3 = {
    index: blockchain.getLastBlock().header.index + 1,
    timestamp: Utils.now(),
    transactions: [new Transaction('address2', 'address1', 10)]
  }
  const nonBankPrivkey = Crypto.randomBytes(32)
  const nonBanPubkey = secp256k1.publicKeyCreate(nonBankPrivkey)
  const nonBankPubkeyHex = nonBanPubkey.toString('hex')

  blockchain.addBlock(new Block({ index: block3.index, timestamp: block3.timestamp }, block3.transactions, bank.bankPubKey), nonBankPrivkey)
  console.log(bank.isChainValid() ? 'Blockchain valida' : 'Blockchain NO valida')

  console.log('----- Not bank signature ----')

  blockchain.addBlock(new Block({ index: block3.index, timestamp: block3.timestamp }, block3.transactions, nonBankPubkeyHex), nonBankPrivkey)
  console.log(bank.isChainValid() ? 'Blockchain valida' : 'Blockchain NO valida')
  console.log('chain', JSON.stringify(blockchain.chain, null, 4))
}

main()
