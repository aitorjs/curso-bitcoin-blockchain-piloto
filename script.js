// https://stackoverflow.com/a/1590264 Es un stack (first in, last out)
const Utils = require('./Utils')
const secp256k1 = require('secp256k1')

class Script {
  constructor(script, tx) {
    this.script = script
    this.tx = tx
    this.stack = []
    this.evaluate()
  }

  evaluate() {
    // console.log('bbb', this.script)
    this.script = this.script.split(' ')

    for (let s of this.script) {
      if (!s.startsWith('OP_')) {
        this.stack.push(s)
        continue
      } else {
        switch (s) {
          case 'OP_DUP':
            this.stack.push(this.stack.slice(-1)[0])
            continue
          case 'OP_HASH160':
            const pop = this.stack.pop()
            this.stack.push(Utils.fromPubKeyToPubKeyHash(pop))
            continue
          case 'OP_EQUALVERIFY':
            let pubkeyhash1 = this.stack.slice(-1)[0]
            let pubkeyhash2 = this.stack.slice(-2)[0]
            // console.log('STACK!!!', this.stack)
            
            // console.log('pubkeyhash1 on OP_EQUALVERIFY', pubkeyhash1)
            // console.log('pubkeyhash1 on OP_EQUALVERIFY', pubkeyhash2)
            if (pubkeyhash1 === pubkeyhash2) {
              this.stack.pop()
              this.stack.pop()
              continue
            } else {
              break
            }
          case 'OP_CHECKSIG':
            let sig = Buffer.from(this.stack.slice(-2)[0], 'hex')          
            let pubkey = Buffer.from(this.stack.slice(-1)[0], 'hex')

            // console.log('--> input', this.tx.input[0].puntero)
            // console.log('--> output', this.tx.output)
            // delete this.tx.input[0].scriptSig
            // unset()
            // console.log('<<< transactions', this.tx.vin[0].txId, this.tx.vin[0].vout.amount , this.tx.vin[0].vout.n, this.tx.vin[0].vout.scriptPubKey, this.tx.vin[0].vout.address, this.tx.vout[0].amount, this.tx.vout[0].address, this.tx.vout[0].scriptPubKey, this.tx.locktime, pubkey)
            // let msg = Buffer.from(Utils.sha256(JSON.stringify(this.tx.input[0].txId + this.tx.input[0].puntero.amount + this.tx.input[0].puntero.n + this.tx.input[0].puntero.scriptPubKey + this.tx.input[0].puntero.address + this.tx.output[0].amount + this.tx.output[0].address + this.tx.output[0].scriptPubKey + this.tx.locktime + pubkey)), 'hex');
            let str = this.tx.input[0].txid + this.tx.input[0].puntero[0].amount + this.tx.input[0].puntero[0].n + this.tx.input[0].puntero[0].scriptPubKey + this.tx.input[0].puntero[0].address + JSON.stringify(this.tx.output) + this.tx.locktime + pubkey.toString('hex')
            // console.log('str on script', str)
            let msg = Buffer.from(Utils.sha256(str), 'hex')
            // console.log('verify sign', secp256k1.verify(msg, sig, pubkey));

            if (secp256k1.verify(msg, sig, pubkey) === true) {
              this.stack.pop()
              this.stack.pop()
              this.stack.push(true)
              continue
            } else {
              break
            }
          default:
            // console.log('OP_code is not implemented')
            break
        }
      }
      return this.stack
    }
  }
}

module.exports = Script
